const express = require('express');
const router = express.Router();
// middlewares add
const logger = require('../middlewares/logger');

router.get('/', logger, (req, res) => {
	// res.send('ROOT로 요청하셨군요. 후후후..');
	res.render('index', {
		greet: '오늘은 더운 날인 것 같군요...',
		author: '김지훈'
	});
});

module.exports = router;