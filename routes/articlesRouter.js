// const express = require('express');
// const router = express.Router();
const router = require('express').Router();
const Article = require('../schemas/article');

// promise로 작성. BUT, async/away code로 바꾸면 debug가 쉬워진다.(breakpoint+)
// router.get('/', (req, res) => {
// 	Article.find({})
// 	.then((articles) => {
// 		// res.send('글전체보기');
// 		res.json(articles);
// 	})
// 	.catch((err) => {
// 		res.send('error', err);
// 	});
// });

router.get('/', async (req, res) => {
	try {
		const articles = await Article.find({})
		res.json(articles);
	}
	catch ( err ) {
		res.send(err);
	}
});

// router.post('/', (req, res) => {
// 	// console.log(req.body);
// 	const article = new Article(req.body);
// 	article.save()
// 		.then((result) => {
// 			// res.send('글쓰기');
// 			res.json(result);
// 		})
// 		.catch((err) => {
// 			res.send('error', err);
// 		});
// });

router.post('/', async (req, res) => {
	try {
		const article = new Article(req.body);
		const result = await article.save();
		// res.json(result);
		res.redirect('/articles');
	}
	catch ( err ) {
		res.send(err);
	}
});

router.get('/:num', async (req, res) => {
	// res.send(req.params.num + '번 글상세보기');
	try {
		const result = await Article.find({_id: req.params.num});
		res.json(result);	// 여러 개가 검색 될 수 있기에, 배열로 결과를 가져온다.
	} catch ( err ) {
		res.send(err);
	}
});

router.put('/:num', async (req, res) => {
	// res.send(req.params.num + '번 글수정');
	try {
		const result = await Article.update(
			{
				_id: req.params.num
			},
			{
				content: req.body.content,
				title: req.body.title
			});
		res.redirect('/articles');	// 수정 후, 전체글보기 결과를 가져온다
		// res.json(result);
	} catch ( err ) {
		res.send(err);
	}
});

router.delete('/:num', async (req, res) => {
	// res.send(req.params.num + '번 글삭제');
	try {
		const result = await Article.remove({_id: req.params.num});
		// res.json(result);	// 이러한 코드보다는 아래처럼
		// res.redirect('http://localhost:27017/articles');	// 생략가능
		// res.redirect('/articles');	// 삭제 후, 전체글보기 결과를 가져온다
		res.json(result);
	} catch ( err ) {
		res.send(err);
	}
});

module.exports = router;
