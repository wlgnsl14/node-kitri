const express = require('express');
const logger = require('morgan');
const bodyParser = require('body-parser');
const path = require('path');
const homeRouter = require('./routes/homeRouter');
const articlesRouter = require('./routes/articlesRouter');
const connect = require('./schemas');

const app = express();
connect();

// view engine setting
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);

// middle wares..
app.use(logger('dev'));
app.use('/static', express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

// routes
app.use('/', homeRouter);
app.use('/articles', articlesRouter);

// send error
app.use((req, res, next) => {
	res.send('404 Not Found.');
});

// server start
app.listen(3030, () => { console.log('server ready...')});